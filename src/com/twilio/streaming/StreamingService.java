package com.twilio.streaming;

import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.logging.Logger;

import com.google.api.services.bigquery.Bigquery;
import com.google.api.services.bigquery.model.TableDataInsertAllRequest;
import com.google.api.services.bigquery.model.TableDataInsertAllResponse;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.google.gson.stream.JsonReader;

public class StreamingService {

	private static final Logger logger = Logger
			.getLogger(StreamingService.class.getCanonicalName());

	public static Iterator<TableDataInsertAllResponse> run(
			final String projectId, final String datasetId,
			final String tableId, final JsonReader rows) throws IOException {

		final Bigquery bigquery = BigqueryServiceFactory.getService();
		final Gson gson = new Gson();
		rows.beginArray();

		return new Iterator<TableDataInsertAllResponse>() {

			/**
			 * Check whether there is another row to stream.
			 *
			 * @return True if there is another row in the stream
			 */
			public boolean hasNext() {
				try {
					return rows.hasNext();
				} catch (IOException e) {
					e.printStackTrace();
				}
				return false;
			}

			/**
			 * Insert the next row, and return the response.
			 *
			 * @return Next page of data
			 */
			public TableDataInsertAllResponse next() {
				try {
					Map<String, Object> rowData = gson
							.<Map<String, Object>> fromJson(rows,
									(new HashMap<String, Object>()).getClass());
					return streamRow(bigquery, projectId, datasetId, tableId,
							new TableDataInsertAllRequest.Rows()
									.setJson(rowData));
				} catch (JsonSyntaxException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}
				return null;
			}

			public void remove() {
				this.next();
			}

			public TableDataInsertAllResponse streamRow(
					final Bigquery bigquery, final String projectId,
					final String datasetId, final String tableId,
					final TableDataInsertAllRequest.Rows row)
					throws IOException {

				return bigquery
						.tabledata()
						.insertAll(
								projectId,
								datasetId,
								tableId,
								new TableDataInsertAllRequest()
										.setRows(Collections.singletonList(row)))
						.execute();
			}

		};

	}

}
