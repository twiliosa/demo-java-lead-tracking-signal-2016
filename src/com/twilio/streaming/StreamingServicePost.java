package com.twilio.streaming;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.joda.time.DateTime;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.api.client.http.AbstractInputStreamContent;
import com.google.api.client.http.ByteArrayContent;
import com.google.api.client.util.Throwables;
import com.google.api.services.bigquery.Bigquery;
import com.google.api.services.bigquery.model.Job;
import com.google.api.services.bigquery.model.JobConfiguration;
import com.google.api.services.bigquery.model.JobConfigurationLoad;
import com.google.api.services.bigquery.model.JobConfigurationTableCopy;
import com.google.api.services.bigquery.model.JobReference;
import com.google.api.services.bigquery.model.Table;
import com.google.api.services.bigquery.model.TableFieldSchema;
import com.google.api.services.bigquery.model.TableReference;
import com.google.api.services.bigquery.model.TableSchema;
import com.google.appengine.labs.repackaged.com.google.common.primitives.Bytes;
import com.google.common.base.Joiner;
import com.sun.rowset.internal.InsertRow;

public class StreamingServicePost {

	private static final Logger logger = Logger
			.getLogger(StreamingService.class.getCanonicalName());

	static Bigquery bigquery = null;

	private static final String JSON_SOURCE_FORMAT = "NEWLINE_DELIMITED_JSON";
	private static final String JSON_CONTENT_TYPE = "application/octet-stream";

	public static void insertData(byte[] jsonData, String tableName, String dataSet) {

		try {
			bigquery = BigqueryServiceFactory.getService();
			if (bigquery == null)
				logger.log(Level.INFO, "bigQuery is null: ");

			insertRows("twilioleadtrackingdemo", dataSet,
					tableName, jsonData);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public static void insertRows(String projectId, String datasetId,
			String tableId, byte[] data) {
		try {

			// Table reference
			TableReference tableReference = new TableReference()
					.setProjectId(projectId).setDatasetId(datasetId)
					.setTableId(tableId);

			logger.log(Level.INFO, tableReference.toPrettyString());

			JobConfigurationLoad loadConfig = new JobConfigurationLoad()
					.setDestinationTable(tableReference).setSourceFormat(
							JSON_SOURCE_FORMAT);
			AbstractInputStreamContent content = new ByteArrayContent(
					JSON_CONTENT_TYPE, data);

			String jobId = runJobWithContent(projectId,
					new JobConfiguration().setLoad(loadConfig), content);

			logger.log(Level.INFO, jobId);

		} catch (IOException e) {
			throw Throwables.propagate(e);
		}
	}

	private static String runJobWithContent(String projectId,
			JobConfiguration jobConfiguration,
			AbstractInputStreamContent content) throws IOException {
		Job job = new Job().setConfiguration(jobConfiguration);
		Job runningJob = content != null ? bigquery.jobs()
				.insert(projectId, job, content).execute() : bigquery.jobs()
				.insert(projectId, job).execute();
		return runningJob.getJobReference().getJobId();
	}
}
