package com.twilio.streaming;

import com.google.api.services.bigquery.Bigquery;
import com.google.api.services.bigquery.BigqueryScopes;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeRequestUrl;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeTokenRequest;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.googleapis.auth.oauth2.GoogleTokenResponse;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.bigquery.Bigquery;
import com.google.api.services.bigquery.BigqueryScopes;
import com.twilio.sdk.auth.AccessToken;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.security.GeneralSecurityException;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * This class creates our Service to connect to Bigquery including auth.
 */
public final class BigqueryServiceFactory {

	private static final Logger logger = Logger
			.getLogger(BigqueryServiceFactory.class.getCanonicalName());

	private static final String CLIENT_SECRET = "./clientsecret.json";
	private static final String P12File = "WEB-INF/clientsecret.p12";
	
	private static String CLIENT_ID = "108116456313035584542";
	static GoogleClientSecrets clientSecrets;

	/**
	 * Private constructor to disable creation of this utility Factory class.
	 */
	private BigqueryServiceFactory() {

	}

	/**
	 * Singleton service used through the app.
	 */
	private static Bigquery service = null;

	/**
	 * Mutex created to create the singleton in thread-safe fashion.
	 */
	private static Object serviceLock = new Object();

	/**
	 * Threadsafe Factory that provides an authorized Bigquery service.
	 * 
	 * @return The Bigquery service
	 * @throws IOException
	 *             Thronw if there is an error connecting to Bigquery.
	 */
	public static Bigquery getService() throws IOException {
		if (service == null) {
			synchronized (serviceLock) {
				if (service == null) {
					service = createAuthorizedClient();
				}
			}
		}
		return service;
	}

	/**
	 * Creates an authorized client to Google Bigquery.
	 *
	 * @return The BigQuery Service
	 * @throws IOException
	 *             Thrown if there is an error connecting
	 */
	// [START get_service]
	private static Bigquery createAuthorizedClient() throws IOException {
		// Create the credential
		HttpTransport transport = new NetHttpTransport();
		JsonFactory jsonFactory = new JacksonFactory(); //
		GoogleCredential credential = null;
		// credential = GoogleCredential.getApplicationDefault(transport,
		// jsonFactory);
		Collection<String> bigqueryScopes = BigqueryScopes.all();

		clientSecrets = loadClientSecrets();

	      logger.log(Level.INFO, clientSecrets.toPrettyString());

//		GoogleAuthorizationCodeFlow flow = new GoogleAuthorizationCodeFlow.Builder(
//			    new NetHttpTransport(),
//			    new JacksonFactory(),
//			    CLIENT_ID, // This comes from your Developers Console project
//			    CLIENT_SECRET, // This, as well
//			    Collections.singleton(BigqueryScopes.BIGQUERY))
//			    .setApprovalPrompt("force")
//			    // Set the access type to offline so that the token can be refreshed.
//			    // By default, the library will automatically refresh tokens when it
//			    // can, but this can be turned off by setting
//			    // dfp.api.refreshOAuth2Token=false in your ads.properties file.
//			    .setAccessType("offline").build();
		
		
		
		try {
			credential = new GoogleCredential.Builder()
					.setTransport(transport)
					.setJsonFactory(jsonFactory)
					.setServiceAccountId("twilioleadtrackingbigquery@twilioleadtrackingdemo.iam.gserviceaccount.com")
					.setServiceAccountPrivateKeyFromP12File(new java.io.File(P12File))
					.setServiceAccountScopes(bigqueryScopes).build();
		} catch (GeneralSecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}


	

		return new Bigquery.Builder(transport, jsonFactory, credential)
				.setApplicationName("twilioleadbigquery")
				.setHttpRequestInitializer(credential).build();
	}
	// [END get_service]
	
	  private static GoogleClientSecrets loadClientSecrets() {
		    try {
		      GoogleClientSecrets clientSecrets =
		    		  GoogleClientSecrets.load(new JacksonFactory(),
		    				  new InputStreamReader(BigqueryServiceFactory.class.getResourceAsStream(CLIENT_SECRET)));
		      return clientSecrets;
		    } catch (Exception e)  {
		      logger.log(Level.INFO,"Could not load clientsecrets.json");
		      e.printStackTrace();
		    }
		    return clientSecrets;
		  }

}
