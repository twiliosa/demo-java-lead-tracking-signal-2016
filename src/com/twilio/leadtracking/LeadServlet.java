// Copyright 2011, Google Inc. All Rights Reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package com.twilio.leadtracking;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.twilio.streaming.StreamingService;
import com.twilio.streaming.StreamingServicePost;
import com.twilio.util.Constants;
import com.twilio.util.Util;

/**
 * This servlet responds to the request corresponding to users. The class
 * creates and manages the User Entity
 * 
 * @author
 */
@SuppressWarnings("serial")
public class LeadServlet extends BaseServlet {

	private static final Logger logger = Logger.getLogger(LeadServlet.class
			.getCanonicalName());

	/**
	 * Get the requested lead entities in JSON format
	 */
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {

		super.doGet(req, resp);
		logger.log(Level.INFO, "Obtaining lead information");
		String searchFor = req.getParameter("q");
		PrintWriter out = resp.getWriter();
		Iterable<Entity> entities = null;
		if (searchFor == null || searchFor.equals("")) {
			entities = LeadEntity.getAllleads();
			out.println(Util.writeJSON(entities));
		} else {
			entities = LeadEntity.getlead(searchFor);
			out.println(Util.writeJSON(entities));
		}
		return;
	}

	/**
	 * Insert the new lead
	 */
	protected void doPut(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		logger.log(Level.INFO, "Creating lead");
		String nameKey = req.getParameter("m-name");
		String phnno = req.getParameter("m-phonenumber");
		String selectedOption = req.getParameter("m-selectsport");
		String countryCode = req.getParameter("m-countrycode");
		logger.log(Level.INFO, "leadID: " + nameKey);
		LeadEntity.createOrUpdatelead(nameKey, phnno, selectedOption, countryCode);

		Lead newLead = new Lead(nameKey, phnno, selectedOption,
				com.twilio.util.Constants.DEFAULT_CITY, countryCode);
		// Lead Creation Event - send lead data to stream
		logger.log(Level.INFO, "leadIDJSON: " + newLead);

		byte[] leadAsJsonBytes = newLead.toJsonAsBytes();
		StreamingServicePost.insertData(leadAsJsonBytes,
				com.twilio.util.Constants.LEAD_DATA_TABLE_BQ, com.twilio.util.Constants.LEAD_TRACKING_DATASET_BQ);
	}

	/**
	 * Delete the lead
	 */
	protected void doDelete(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		String leadName = req.getParameter("id");
		logger.log(Level.INFO, "Deleting User {0}", leadName);
		Key key = KeyFactory.createKey("leadname", leadName);
		try {
			// CASCADE_ON_DELETE
			Iterable<Entity> entities = Util.listChildKeys("Order", key);
			final List<Key> orderkeys = new ArrayList<Key>();
			final List<Key> linekeys = new ArrayList<Key>();
			for (Entity e : entities) {
				orderkeys.add(e.getKey());
				Iterable<Entity> lines = Util.listEntities("LineItem",
						"orderID", String.valueOf(e.getKey().getId()));
				for (Entity en : lines) {
					linekeys.add(en.getKey());
				}
			}
			Util.deleteEntity(linekeys);
			Util.deleteEntity(orderkeys);
			Util.deleteEntity(key);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Redirect the call to doDelete or doPut method
	 */
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		String action = req.getParameter("action");
		if (action.equalsIgnoreCase("delete")) {
			doDelete(req, resp);
			return;
		} else if (action.equalsIgnoreCase("put")) {
			doPut(req, resp);
			return;
		}
	}

	public String processLead() {

		// lookup the city from the Signal database

		return com.twilio.util.Constants.DEFAULT_CITY;
	}

}
