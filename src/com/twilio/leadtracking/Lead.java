package com.twilio.leadtracking;

import java.io.IOException;
import java.sql.Timestamp;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

public class Lead {

	private final static ObjectMapper JSON = new ObjectMapper();
	static {
		JSON.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
	}
	private String name;
	private String leadphonenumber;
	private String selectedoption;
	private String timestamp;
	private String leadcity;
	private String countrycode;

	public Lead() {

	}

	public Lead(String name, String leadphonenumber, String selectedoption, String leadcity, String countryCode) {
		this.name = name;
		this.leadphonenumber = leadphonenumber;
		this.selectedoption = selectedoption;
		this.timestamp = new Timestamp(new java.util.Date().getTime())
				.toString();
		this.leadcity = leadcity;
		this.countrycode = countryCode;
	}

	public String getName() {
		return name;
	}

	public String getleadphonenumber() {
		return leadphonenumber;

	}

	public String getselectedoption() {
		return selectedoption;
	}

	public String gettimestamp() {
		return timestamp;
	}

	public String getleadcity() {
		return leadcity;
	}
	
	public String getcountrycode() {
		return countrycode;
	}

	public byte[] toJsonAsBytes() {
		try {
			return JSON.writeValueAsBytes(this);
		} catch (IOException e) {
			return null;
		}
	}

}
