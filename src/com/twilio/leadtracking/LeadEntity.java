// Copyright 2011, Google Inc. All Rights Reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package com.twilio.leadtracking;

import java.util.Iterator;

import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.common.collect.Iterators;
import com.twilio.util.Util;
import com.twilio.util.UtilHelper;

/**
 * This class defines the methods for basic operations of create, update &
 * retrieve for Lead Entity
 * 
 * @author
 *
 */
public class LeadEntity {

	/**
	 * Checks if the entity is existing and if it is not, it creates the entity
	 * else it updates the entity
	 * 
	 * @param name
	 *            : name for the lead
	 * @param leadid
	 *            : leadid of the leadid
	 * @param phnno
	 *            : phone no
	 */
	public static void createOrUpdatelead(String name, String phnno,
			String selectedOption, String countryCode) {
		Entity lead = getSinglelead(name);
		if (lead == null) {
			lead = new Entity("lead", phnno);
			lead.setProperty("name", name);
			lead.setProperty("leadid", UtilHelper.getUniqueID());
			lead.setProperty("phnno", phnno);
			lead.setProperty("selectedoption", selectedOption);
			lead.setProperty("countrycode", countryCode);
		}
		Util.persistEntity(lead);
	}

	/**
	 * List all the leads available
	 * 
	 * @return an iterable list with all the leads
	 */
	public static Iterable<Entity> getAllleads() {
		Iterable<Entity> entities = Util.listEntities("lead", null, null);
		return entities;
	}

	/**
	 * Searches for a leads and returns the entity as an iterable The search is
	 * performed by creating a query and searching for the attribute
	 * 
	 * @param saleisonName
	 *            : username of the leads
	 * @return iterable with� the leads searched for
	 */
	public static Iterable<Entity> getlead(String phnno) {
		Iterable<Entity> entities = Util.listEntities("lead", "phnno", phnno);
		return entities;
	}

	/**
	 * Searches for a leads and returns the entity as an iterable The search is
	 * key based instead of query
	 * 
	 * @param saleisonName
	 *            : name of the lead
	 * @return the entity with the name as key
	 */
	public static Entity getSinglelead(String phnno) {
		Key key = KeyFactory.createKey("lead", phnno);
		return Util.findEntity(key);
	}

	public static int getLeadCount() {
		Iterable<Entity> entities = Util.listEntities("lead", null, null);
		Iterator<Entity> leadItr = entities.iterator();
		int size = Iterators.size(leadItr);

		return size;

	}
}
