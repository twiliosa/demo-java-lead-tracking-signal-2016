package com.twilio.util;


public class Constants {

	public static String ACCOUNT_SID = "ACac21b4749127eec9f471d41f87683855";
	public static String AUTH_TOKEN = "788fc4795e97c3d295ffdeabb8cce301";
	public static String DEFAULT_PHONENUMBER = "+14153602696";
	public static String PARTITION_LEAD = "LEAD";
	public static String EVENT_CALLMAPPING = "CallMapping";
	public static String EVENT_LEAD = "Lead";
	public static String STREAM_URL = "http://twilioleadtracking-env.us-east-1.elasticbeanstalk.com/streamevent";
	public static String DEFAULT_CITY = "San Francisco";
	
	public static String LEAD_TRACKING_DATASET_BQ = "twilioleadtrackingdataset";
	public static String LEAD_DATA_TABLE_BQ = "twilioleaddata";
	
	public static String PHONENUMBER_DATA_TABLE_BQ = "twilioleadphonenumber";
	public static String PHONENUMBER_DATASET_BQ = "twilioleadtrackingphonenumber";
	
	public static String CALLSTATE_DATA_TABLE_BQ = "twilioleadcallstate";
	public static String CALLSTATE_DATASET_BQ = "twilioleadtrackingcallstate";	
	
	public static String US_CC = "+1";

	public static String CALL_STATUS_EVENTS_URL = "http://kedar.ngrok.io/callstatus";
	
	public static String WPP_URL = "https://proapi.whitepages.com/2.2/phone.json?api_key=3004c8acd188f32ffaa292fa95371582&phone_number=";
	public static String CALL_EXPERIENCE_URL = "http://kedar.ngrok.io/callexperience?agent=";
	
	public static int MAX_AGENTS = 0;
	public static String CALL_EXPERIENCE_AGENT = "agent";
	public static String CALL_EXPERIENCE_GREETING = "greeting";
	
	public static String AGENT1_PHONENUMBER = "+12405156352";
	public static String AGENT2_PHONENUMBER = "+12405156352";
//	public static String AGENT3_PHONENUMBER = "";
//	public static String AGENT4_PHONENUMBER = "";
//	public static String AGENT5_PHONENUMBER = "";


}
