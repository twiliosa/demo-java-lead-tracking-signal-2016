package com.twilio.calls;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import com.google.appengine.api.datastore.Entity;
import com.google.common.collect.Iterables;
import com.google.common.collect.Iterators;
import com.twilio.leadtracking.LeadEntity;
import com.twilio.sdk.TwilioRestClient;
import com.twilio.sdk.TwilioRestException;
import com.twilio.sdk.resource.factory.CallFactory;
import com.twilio.sdk.resource.instance.Call;
import com.twilio.streaming.StreamingService;
import com.twilio.calls.CallMappings;

public class TwilioLeadTrackingCalls {

	private static final Logger logger = Logger
			.getLogger(TwilioLeadTrackingCalls.class.getCanonicalName());

	public static void setOutboundCalls(Map<String, String> leadPhoneNumberMap,
			Map<String, String> agentCallListMap) {

		TwilioRestClient client = new TwilioRestClient(
				com.twilio.util.Constants.ACCOUNT_SID,
				com.twilio.util.Constants.AUTH_TOKEN);

		logger.log(Level.INFO, "callLEADS in HERE");

		// Build a filter for the CallList
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		Iterator<Map.Entry<String, String>> iterator = leadPhoneNumberMap
				.entrySet().iterator();

		// connect first 5 people to agents
		int agentExpCounter = 1;
		String experienceURL;

		while (iterator.hasNext()) {

			Map.Entry<String, String> entry = iterator.next();
			logger.log(Level.INFO, entry.getKey());
			logger.log(Level.INFO, entry.getValue());

			String calleePhoneNumber = entry.getKey();

			Entity leadEntity = LeadEntity.getSinglelead(calleePhoneNumber);
			String calleeName = leadEntity.getProperty("name").toString();
			logger.log(Level.INFO, "CALLEE NAME" +calleeName);

			if (agentExpCounter <= com.twilio.util.Constants.MAX_AGENTS) {
				String agentKey = "agent" + String.valueOf(agentExpCounter);
				experienceURL = com.twilio.util.Constants.CALL_EXPERIENCE_URL
						+ agentCallListMap.get(agentKey) + "&callee="
						+ calleeName;
				agentExpCounter++;
				logger.log(Level.INFO, experienceURL);

			} else
				experienceURL = com.twilio.util.Constants.CALL_EXPERIENCE_URL
						+ "machine"+"&callee="+calleeName;

			// Set outbound phone calls
			params.add(new BasicNameValuePair("To", entry.getKey()));
			params.add(new BasicNameValuePair("From", entry.getValue()));
			params.add(new BasicNameValuePair("Url", experienceURL));
			params.add(new BasicNameValuePair("StatusCallback",
					com.twilio.util.Constants.CALL_STATUS_EVENTS_URL));
			params.add(new BasicNameValuePair("StatusCallbackMethod", "POST"));
			params.add(new BasicNameValuePair("StatusCallbackEvent",
					"initiated"));
			params.add(new BasicNameValuePair("StatusCallbackEvent", "ringing"));
			params.add(new BasicNameValuePair("StatusCallbackEvent", "answered"));
			params.add(new BasicNameValuePair("StatusCallbackEvent",
					"completed"));
			String areaCode = com.twilio.util.UtilHelper.getAreaCode(
					entry.getKey(), "US");

			CallFactory callFactory = client.getAccount().getCallFactory();
			Call call = null;
			try {
				call = callFactory.create(params);
				CallMappings callMap = new CallMappings(entry.getKey(),
						entry.getValue(), areaCode);
				logger.log(Level.INFO, call.getSid());

			} catch (TwilioRestException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

	}

}
