package com.twilio.calls.states;

import java.io.IOException;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

public class CallState {

	private final static ObjectMapper JSON = new ObjectMapper();
	static {
		JSON.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
	}
	private String callsid;
	private String callduration;
	private String recordingUrl;

	public CallState(String callsid, String callduration, String recordingUrl) {
		this.callsid = callsid;
		this.callduration = callduration;
		this.recordingUrl = recordingUrl;
	}

	public byte[] toJsonAsBytes() {
		try {
			return JSON.writeValueAsBytes(this);
		} catch (IOException e) {
			return null;
		}
	}

}
