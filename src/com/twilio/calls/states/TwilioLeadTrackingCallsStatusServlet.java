package com.twilio.calls.states;

import java.io.IOException;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.appengine.api.datastore.Entity;
import com.twilio.calls.TwilioLeadTrackingCallsServlet;
import com.twilio.leadtracking.BaseServlet;
import com.twilio.leadtracking.LeadEntity;
import com.twilio.streaming.StreamingServicePost;
import com.twilio.util.Util;

public class TwilioLeadTrackingCallsStatusServlet extends BaseServlet {

	private static final Logger logger = Logger
			.getLogger(TwilioLeadTrackingCallsServlet.class.getCanonicalName());

	//
	// private static int initiatedCount = 0;
	// private static int ringingCount = 0;
	// private static int inprogressCount = 0;
	// private static int completedCount = 0;

	public void service(HttpServletRequest request, HttpServletResponse response)
			throws IOException {

		String callStatus = request.getParameter("CallStatus");
		String callSid = request.getParameter("CallSid");

		logger.log(Level.INFO, "call Status: " + callStatus);

		Entity callStateEntity = CallStateEntity
				.getSingleCallState("callstate");
		if (callStateEntity == null) {
			CallStateEntity.createOrUpdateCallStates(0, 0, 0, 0);
		}

		callStateEntity = CallStateEntity.getSingleCallState("callstate");

		if (callStatus.equalsIgnoreCase("initiated")) {
			String initiated = callStateEntity.getProperty("initiatedCount")
					.toString();
			Integer initiatedInt = Integer.valueOf(initiated);
			initiatedInt++;
			callStateEntity.setProperty("initiatedCount",
					initiatedInt.toString());

		} else if (callStatus.equalsIgnoreCase("ringing")) {
			String initiated = callStateEntity.getProperty("initiatedCount")
					.toString();
			Integer initiatedInt = Integer.valueOf(initiated);
			Integer ringingInt = Integer.valueOf(callStateEntity.getProperty(
					"ringingCount").toString());
			initiatedInt--;
			ringingInt++;
			callStateEntity.setProperty("initiatedCount",
					initiatedInt.toString());
			callStateEntity.setProperty("ringingCount", ringingInt.toString());

		} else if (callStatus.equalsIgnoreCase("in-progress")) {
			String inprogress = callStateEntity.getProperty("inprogressCount")
					.toString();
			Integer inprogressInt = Integer.valueOf(inprogress);
			Integer ringingInt = Integer.valueOf(callStateEntity.getProperty(
					"ringingCount").toString());
			inprogressInt++;
			ringingInt--;
			callStateEntity.setProperty("inprogressCount",
					inprogressInt.toString());
			callStateEntity.setProperty("ringingCount", ringingInt.toString());

		} else if (callStatus.equalsIgnoreCase("completed")) {
			String completed = callStateEntity.getProperty("completedCount")
					.toString();
			Integer completedInt = Integer.valueOf(completed);
			Integer inprogressInt = Integer.valueOf(callStateEntity
					.getProperty("inprogressCount").toString());
			completedInt++;
			inprogressInt--;
			callStateEntity.setProperty("completedCount",
					completedInt.toString());
			callStateEntity.setProperty("inprogressCount",
					inprogressInt.toString());

		}
		Util.persistEntity(callStateEntity);

		// Get latest object from DataStore then update count

		// if queued, then its a new outbound call
		// if initiated, then +1 to initiated and -1 from queued
		// if ringing, then +1 to ringing and -1 from initiated
		// if answered, then +1 to answered and -1 from ringing
		// if completed, then +1 to completed and -1 from answered

		// if (callStatus.equalsIgnoreCase("initiated")) {
		// initiatedCount++;
		//
		// } else if (callStatus.equalsIgnoreCase("ringing")) {
		// ringingCount++;
		// initiatedCount--;
		// if(initiatedCount<0)
		// initiatedCount = 0;
		// } else if (callStatus.equalsIgnoreCase("in-progress")) {
		// inprogressCount++;
		// ringingCount--;
		// if(ringingCount<0)
		// ringingCount = 0;
		// } else if (callStatus.equalsIgnoreCase("completed")) {
		// completedCount++;
		// inprogressCount--;
		// if(inprogressCount<0)
		// inprogressCount = 0;
		//
		// // Update call object with CallStatus values
		// // CallState callState = new CallState(callSid,
		// // request.getParameter("callStatus"),
		// // request.getParameter("recordingUrl"));
		//
		// // byte[] jsonAsBytes = callState.toJsonAsBytes();
		// // // push to BigQuery
		// // StreamingServicePost.insertData(jsonAsBytes,
		// // com.twilio.util.Constants.CALLSTATE_DATA_TABLE_BQ,
		// // com.twilio.util.Constants.CALLSTATE_DATASET_BQ);
		//
		// }

		// Add counts to a database
		// CallStateEntity.createOrUpdateCallStates(initiatedCount,
		// ringingCount, inprogressCount, completedCount);

	}
}
