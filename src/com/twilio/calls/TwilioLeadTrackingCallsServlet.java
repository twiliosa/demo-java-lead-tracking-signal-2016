package com.twilio.calls;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.memcache.ErrorHandlers;
import com.google.appengine.api.memcache.MemcacheService;
import com.google.appengine.api.memcache.MemcacheServiceFactory;
import com.twilio.leadtracking.BaseServlet;
import com.twilio.leadtracking.LeadEntity;
import com.twilio.lookup.PhoneNumberLookup;
import com.twilio.lookup.PhoneNumberPool;

public class TwilioLeadTrackingCallsServlet extends BaseServlet {

	private static final Logger logger = Logger
			.getLogger(TwilioLeadTrackingCallsServlet.class.getCanonicalName());

	public void service(HttpServletRequest request, HttpServletResponse response)
			throws IOException {

		ArrayList<String> leadPhoneNumberAl = getLeadPhoneNumbers();

		// Create "From", "To" mapping pairs and then set the outbound calls
		Map<String, String> callListToFrom = PhoneNumberPool
				.createToFromPairs(leadPhoneNumberAl);
		Map<String, String> agentCallListMap = prepareAgentCallList();
		runPhoneNumberLookup(leadPhoneNumberAl);

		// All the Keys are phone numbers.
		callLeads(callListToFrom, agentCallListMap);

	}

	public ArrayList<String> getLeadPhoneNumbers() {

		ArrayList<String> leadPhoneNumberAl = new ArrayList<String>();
		Iterable<Entity> leads = LeadEntity.getAllleads();
		Iterator<Entity> leadItr = leads.iterator();

		while (leadItr.hasNext()) {
			Entity lead = leadItr.next();
			String phoneNumber = lead.getProperty("phnno").toString();
			leadPhoneNumberAl.add(phoneNumber);
		}

		return leadPhoneNumberAl;

	}

	public void callLeads(Map<String, String> leadPhoneNumberMap,
			Map<String, String> agentCallListMap) {
		logger.log(Level.INFO, "callLEADS");

		TwilioLeadTrackingCalls.setOutboundCalls(leadPhoneNumberMap,
				agentCallListMap);
	}

	public void runPhoneNumberLookup(ArrayList<String> leadPhoneNumbersAl) {

		// run the lookup against all lead phone numbers and then pipe the data
		// to the stream
		logger.log(Level.INFO, String.valueOf(leadPhoneNumbersAl.size()));

		Iterator<String> leadPhoneNumberItr = leadPhoneNumbersAl.iterator();
		while (leadPhoneNumberItr.hasNext()) {
			PhoneNumberLookup.lookupPhoneNumber(leadPhoneNumberItr.next());
		}

	}

	public Map<String, String> prepareAgentCallList() {

		// load up agent phone numbers in memory
		Map<String, String> agentCallList = new HashMap<String, String>();
		agentCallList.put("agent1",
				com.twilio.util.Constants.AGENT1_PHONENUMBER);
		agentCallList.put("agent2",
				com.twilio.util.Constants.AGENT2_PHONENUMBER);
		// agentCallList.put("agent3",
		// com.twilio.util.Constants.AGENT3_PHONENUMBER);
		// agentCallList.put("agent4",
		// com.twilio.util.Constants.AGENT4_PHONENUMBER);
		// agentCallList.put("agent5",
		// com.twilio.util.Constants.AGENT5_PHONENUMBER);

		return agentCallList;

	}

}
