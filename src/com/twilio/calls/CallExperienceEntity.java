package com.twilio.calls;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.common.collect.Iterables;
import com.twilio.lookup.PhoneNumberCarrier;
import com.twilio.util.Util;

public class CallExperienceEntity {
	
	private static final Logger logger = Logger
			.getLogger(CallExperienceEntity.class.getCanonicalName());

	public static void createOrUpdateCallExperience(String experiencetype) {

		Entity experienceEntity = getSingleCallExperience(experiencetype);
		if (experienceEntity == null) {

			experienceEntity = new Entity("experiencetype", experiencetype);
			experienceEntity.setProperty("experiencetype", experiencetype);
			experienceEntity.setProperty("count", 1);
		} else {

			String currentCount = experienceEntity.getProperty("count")
					.toString();

			int currentCountInt = Integer.valueOf(currentCount);
			currentCountInt++;
			experienceEntity
					.setProperty("count", String.valueOf(currentCountInt));
			
		}
		Util.persistEntity(experienceEntity);

	}

	public static Iterable<Entity> getAllCallExperience() {
		Iterable<Entity> entities = Util
				.listEntities("experiencetype", null, null);
		return entities;
	}

	public static Iterable<Entity> getCallExperience(String experiencetype) {
		Iterable<Entity> entities = Util.listEntities("experiencetype",
				"experiencetype", experiencetype);
		return entities;
	}

	public static Entity getSingleCallExperience(String experiencetype) {
		Key key = KeyFactory.createKey("experiencetype", experiencetype);
		return Util.findEntity(key);
	}
	
	public static Map<String, String> getCallExperienceStats() {

		Iterable<Entity> entities = getAllCallExperience();
		Iterator<Entity> callexperienceItr = entities.iterator();

		Map<String, String> callExperienceMap = new HashMap<String, String>();
		
		while(callexperienceItr.hasNext()) {
			Entity callExperienceEntity = callexperienceItr.next();
			String experienceType = callExperienceEntity.getProperty("experiencetype").toString();
			String count = callExperienceEntity.getProperty("count").toString();
			callExperienceMap.put(experienceType, count);
		}
		
		return callExperienceMap;

	}	

}
