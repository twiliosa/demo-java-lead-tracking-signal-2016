package com.twilio.calls;

import java.io.IOException;
import java.sql.Timestamp;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

public class CallMappings {
	
	private final static ObjectMapper JSON = new ObjectMapper();
	static {
		JSON.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
	}
	private String fromNumber;
	private String toNumber;
	private String timeStamp;
	private String areaCode;


	public CallMappings(String fromNumber,
			String toNumber, String areaCode) {
		this.areaCode = areaCode;
		this.fromNumber = fromNumber;
		this.toNumber = toNumber;
		this.timeStamp = new Timestamp(new java.util.Date().getTime()).toString();
	}

	public byte[] toJsonAsBytes() {
		try {
			return JSON.writeValueAsBytes(this);
		} catch (IOException e) {
			return null;
		}
	}

}
