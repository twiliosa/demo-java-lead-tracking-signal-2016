package com.twilio.lookup;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.common.collect.Iterables;
import com.google.common.collect.Iterators;
import com.twilio.calls.TwilioLeadTrackingCallsServlet;
import com.twilio.util.Util;
import com.twilio.util.UtilHelper;

public class PhoneNumberAreaCode {

	private static final Logger logger = Logger
			.getLogger(PhoneNumberAreaCode.class.getCanonicalName());
	
	public static void createOrUpdateareaCode(String areacode) {

		logger.log(Level.INFO, "AREACODE !!!!!" +areacode);
		
		Entity areacodeEntity = getSingleareaCode(areacode);
		if (areacodeEntity == null) {
			logger.log(Level.INFO, "AREACODE !!!!!  NULL");

			areacodeEntity = new Entity("areacode", areacode);
			areacodeEntity.setProperty("areacode", areacode);
			areacodeEntity.setProperty("count", 1);
		} else {

			String currentCount = areacodeEntity.getProperty("count")
					.toString();
			logger.log(Level.INFO, "AREACODE !!!!! COUNT" +currentCount);

			int currentCountInt = Integer.valueOf(currentCount);
			currentCountInt++;
			areacodeEntity
					.setProperty("count", String.valueOf(currentCountInt));
			
		}
		Util.persistEntity(areacodeEntity);
	}

	public static Iterable<Entity> getAllareaCodes() {
		Iterable<Entity> entities = Util.listEntities("areacode", null, null);
		return entities;
	}

	public static Iterable<Entity> getareaCode(String areaCodeName) {
		Iterable<Entity> entities = Util.listEntities("areacode", "areacode",
				areaCodeName);
		return entities;
	}

	public static Entity getSingleareaCode(String areaCodeName) {
		Key key = KeyFactory.createKey("areacode", areaCodeName);
		return Util.findEntity(key);
	}

	public static int getareaCodeCount() {
		Iterable<Entity> entities = Util.listEntities("areacode", null, null);
		Iterator<Entity> areaCodeItr = entities.iterator();
		int size = Iterators.size(areaCodeItr);

		return size;

	}

	public static Map<String, String> getPhoneNumberAreaCodeStats() {

		Iterable<Entity> entities = getAllareaCodes();
		Iterator<Entity> phoneNumberAreaCodeItr = entities.iterator();
		Map<String, String> phoneNumberAreaCodeCountMap = new HashMap<String, String>();

		while (phoneNumberAreaCodeItr.hasNext()) {

			// query datastore to see if we have covered the carrier
			Entity phonenumberAreaCodeEntity = phoneNumberAreaCodeItr.next();
			if (phonenumberAreaCodeEntity != null) {
				String areaCode = phonenumberAreaCodeEntity.getProperty(
						"areacode").toString();
				String count = phonenumberAreaCodeEntity.getProperty(
						"count").toString();
				phoneNumberAreaCodeCountMap.put(areaCode, count);

			}
			logger.log(Level.INFO, "Total AreaCodes", +Iterables.size(entities));

		}
		return phoneNumberAreaCodeCountMap;

	}

}
