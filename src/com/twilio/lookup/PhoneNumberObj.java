package com.twilio.lookup;

import java.io.IOException;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

public class PhoneNumberObj {

	private final static ObjectMapper JSON = new ObjectMapper();
	static {
		JSON.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
	}
	private String phonenumber;
	private String phonenumbertype;
	private String phonenumbercarrier;
	private String phonenumbercountrycode;

	public PhoneNumberObj() {

	}

	public PhoneNumberObj(String phonenumber, String phonenumber_type,
			String phonenumber_carrier, String phonenumber_countrycode) {
		this.phonenumber = phonenumber;
		this.phonenumbertype = phonenumber_type;
		this.phonenumbercarrier = phonenumber_carrier;
		this.phonenumbercountrycode = phonenumber_countrycode;
	}

	public String getphonenumber() {
		return phonenumber;
	}

	public String getphonenumbertype() {
		return phonenumbertype;

	}

	public String getphonenumbercarrier() {
		return phonenumbercarrier;
	}

	public String getphonenumbercountrycode() {
		return phonenumbercountrycode;
	}

	public byte[] toJsonAsBytes() {
		try {
			return JSON.writeValueAsBytes(this);
		} catch (IOException e) {
			return null;
		}
	}

}
