package com.twilio.lookup;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.appengine.api.datastore.Entity;
import com.twilio.leadtracking.BaseServlet;
import com.twilio.leadtracking.LeadEntity;

public class TwilioPhoneNumberLookupServlet extends BaseServlet {

	private static final Logger logger = Logger
			.getLogger(TwilioPhoneNumberLookupServlet.class.getCanonicalName());

	public void service(HttpServletRequest request, HttpServletResponse response)
			throws IOException {

		ArrayList<String> leadPhoneNumbersAl = getAllLeadPhoneNumbers();
		runPhoneNumberLookup(leadPhoneNumbersAl);
	}

	public ArrayList<String> getAllLeadPhoneNumbers() {

		ArrayList<String> leadPhoneNumberAl = new ArrayList<String>();

		Iterable<Entity> leads = LeadEntity.getAllleads();
		Iterator<Entity> leadItr = leads.iterator();

		logger.log(Level.INFO, "PhoneNumberAL -- LOOOKUP ");

		while (leadItr.hasNext()) {
			Entity lead = leadItr.next();
			String phoneNumber = lead.getProperty("phnno").toString();
			leadPhoneNumberAl.add(phoneNumber);
		}
		logger.log(Level.INFO, String.valueOf(leadPhoneNumberAl.size()));

		return leadPhoneNumberAl;

	}

	public void runPhoneNumberLookup(ArrayList<String> leadPhoneNumbersAl) {

		// run the lookup against all lead phone numbers and then pipe the data
		// to the stream
		logger.log(Level.INFO, String.valueOf(leadPhoneNumbersAl.size()));

		Iterator<String> leadPhoneNumberItr = leadPhoneNumbersAl.iterator();
		while (leadPhoneNumberItr.hasNext()) {
			PhoneNumberLookup.lookupPhoneNumber(leadPhoneNumberItr.next());
		}

	}

}
