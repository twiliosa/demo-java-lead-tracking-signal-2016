package com.twilio.lookup;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.appengine.api.datastore.Entity;
import com.twilio.leadtracking.LeadEntity;
import com.twilio.sdk.TwilioRestClient;
import com.twilio.sdk.resource.instance.IncomingPhoneNumber;
import com.twilio.sdk.resource.list.IncomingPhoneNumberList;
import com.twilio.streaming.StreamingService;
import com.twilio.util.Util;

public class PhoneNumberPool {

	private static final Logger logger = Logger.getLogger(PhoneNumberPool.class
			.getCanonicalName());

	/**
	 * 
	 * @param leadPhoneNumbers
	 * @return
	 */

	public static Map<String, String> createToFromPairs(
			ArrayList<String> leadPhoneNumbers) {

		Map<String, String> callListToFrom = new HashMap<String, String>();

		TwilioRestClient client = new TwilioRestClient(
				com.twilio.util.Constants.ACCOUNT_SID,
				com.twilio.util.Constants.AUTH_TOKEN);

		Iterator<String> alIterator = leadPhoneNumbers.iterator();
		// Loop over numbers and print out a property for each one.
		while (alIterator.hasNext()) {
			// Build a filter for the IncomingPhoneNumberList
			Map<String, String> params = new HashMap<String, String>();
			String toPhoneNumber = alIterator.next();
			params.put("PhoneNumber", toPhoneNumber);

			IncomingPhoneNumberList numbersList = client.getAccount()
					.getIncomingPhoneNumbers(params);

			Iterator<IncomingPhoneNumber> numbersListItr = numbersList
					.iterator();
			String fromPhoneNumber = null;
			if (numbersListItr.hasNext())
				fromPhoneNumber = numbersListItr.next().getPhoneNumber();

			if (fromPhoneNumber == null)
				fromPhoneNumber = com.twilio.util.Constants.DEFAULT_PHONENUMBER;

			callListToFrom.put(toPhoneNumber, fromPhoneNumber);
			addAreaCode(toPhoneNumber);

		}
		return callListToFrom;
	}

	/**
	 * 
	 * @param toPhoneNumber
	 * @param fromPhoneNumber
	 */

	public static void addPhoneNumber(String toPhoneNumber,
			String fromPhoneNumber) {

		Entity phoneNumberToFromEntity = new Entity("phonenumber",
				toPhoneNumber);
		phoneNumberToFromEntity.setProperty("tophonenumber", toPhoneNumber);
		phoneNumberToFromEntity.setProperty("fromphonenumber", fromPhoneNumber);
		Util.persistEntity(phoneNumberToFromEntity);

	}
	
	public static void addAreaCode(String phoneNumber) {
		
		String areaCode = phoneNumber.substring(0,3);
		
		logger.log(Level.INFO, "areaCode" +areaCode);
		PhoneNumberAreaCode.createOrUpdateareaCode(areaCode);
		
	}

}