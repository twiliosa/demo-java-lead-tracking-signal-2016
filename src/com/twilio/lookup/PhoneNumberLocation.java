package com.twilio.lookup;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.common.collect.Iterables;
import com.twilio.util.Util;

public class PhoneNumberLocation {

	public static final Logger logger = Logger
			.getLogger(PhoneNumberEntity.class.getCanonicalName());

	public static void createOrUpdatePhoneNumberLocation(String phonenumber,
			String latitude, String longitude) {

		Entity phoneNumberLocation = new Entity("phonenumberlocation", phonenumber);
		phoneNumberLocation.setProperty("phonenumber", phonenumber);
		phoneNumberLocation.setProperty("latitude", latitude);
		phoneNumberLocation.setProperty("longitude", longitude);

		Util.persistEntity(phoneNumberLocation);

	}

	public static Iterable<Entity> getAllAreaCodeLocations() {
		Iterable<Entity> entities = Util
				.listEntities("phonenumberlocation", null, null);
		return entities;
	}

	public static Iterable<Entity> getAreaCodeLocation(String phoneNumber) {
		Iterable<Entity> entities = Util.listEntities("phonenumberlocation",
				"phonenumber", phoneNumber);
		return entities;
	}

	public static Entity getSingleAreaCodeLocation(String phoneNumber) {
		Key key = KeyFactory.createKey("phonenumberlocation", phoneNumber);
		return Util.findEntity(key);
	}

	public static Map<String, String> getPhoneNumberAddressStats() {

		Iterable<Entity> entities = getAllAreaCodeLocations();
		Iterator<Entity> phoneNumberItr = entities.iterator();
		Map<String, String> addressLatLong = new HashMap<String, String>();

		while (phoneNumberItr.hasNext()) {

			// query datastore to see if we have covered the carrier
			Entity phonenumberlocationEntity = phoneNumberItr.next();
			if (phonenumberlocationEntity != null) {
				String latitude = phonenumberlocationEntity.getProperty(
						"latitude").toString();
				String longitude = phonenumberlocationEntity.getProperty(
						"longitude").toString();
				addressLatLong.put(latitude, longitude);

			}
			logger.log(Level.INFO, "Total Address", +Iterables.size(entities));

		}
		return addressLatLong;

	}

}
