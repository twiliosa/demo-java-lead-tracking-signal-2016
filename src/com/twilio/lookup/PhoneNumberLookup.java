package com.twilio.lookup;

import java.net.URL;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.appengine.api.urlfetch.HTTPHeader;
import com.google.appengine.api.urlfetch.HTTPRequest;
import com.google.appengine.api.urlfetch.HTTPResponse;
import com.google.appengine.api.urlfetch.URLFetchService;
import com.google.appengine.api.urlfetch.URLFetchServiceFactory;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.twilio.sdk.LookupsClient;
import com.twilio.sdk.resource.instance.lookups.PhoneNumber;
import com.twilio.streaming.StreamingServicePost;
import com.twilio.util.Constants;

public class PhoneNumberLookup {

	private static final Logger logger = Logger
			.getLogger(PhoneNumberLookup.class.getCanonicalName());

	public static boolean lookupPhoneNumber(String lookupPhoneNumber) {

		boolean lookupSuccessful = false;

		try {
			LookupsClient client = new LookupsClient(Constants.ACCOUNT_SID,
					Constants.AUTH_TOKEN);

			PhoneNumber phoneNumber = client.getPhoneNumber(lookupPhoneNumber,
					true);

			String phoneNumberType = phoneNumber.getType().toString();
			String phoneNumberCarrier = phoneNumber.getCarrierName();
			String phoneNumberCountryCode = phoneNumber.getCountryCode();

			// Lookup the phone number against Whitepages pro, create a
			// different datastore for it

			lookupSuccessful = true;

			PhoneNumberEntity
					.createOrUpdatePhoneNumber(lookupPhoneNumber,
							phoneNumberType, phoneNumberCarrier,
							phoneNumberCountryCode);
			PhoneNumberCarrier
					.createOrUpdatePhoneNumberCarrier(phoneNumberCarrier);

			PhoneNumberObj phoneNumberObj = new PhoneNumberObj(
					lookupPhoneNumber, phoneNumberType, phoneNumberCarrier,
					phoneNumberCountryCode);

			logger.log(Level.INFO, "PhoneNumber: " + phoneNumberObj);

			byte[] phoneNumberAsJsonBytes = phoneNumberObj.toJsonAsBytes();

			getPhoneNumberLookupWPP(lookupPhoneNumber);

			// StreamingServicePost.insertData(phoneNumberAsJsonBytes,
			// com.twilio.util.Constants.PHONENUMBER_DATA_TABLE_BQ,
			// com.twilio.util.Constants.PHONENUMBER_DATASET_BQ);

		} catch (Exception e) {
			e.printStackTrace();
		}

		return lookupSuccessful;
	}

	public static void getPhoneNumberLookupWPP(String phoneNumber) {

		URLFetchService service = URLFetchServiceFactory.getURLFetchService();

		try {

			HTTPRequest fetchReq = new HTTPRequest(new URL(Constants.WPP_URL
					+ phoneNumber));
			HTTPResponse fetchRes = service.fetch(fetchReq);
			if (fetchRes.getResponseCode() == 200) {

				JsonElement jelement = new JsonParser().parse(new String(
						fetchRes.getContent()));
				JsonObject jObject = jelement.getAsJsonObject();
				JsonArray jArray = jObject.getAsJsonArray("results");

				JsonElement jElementResult = jArray.get(0);
				JsonObject jObjectResult = jElementResult.getAsJsonObject();
				JsonElement jElementLocation = jObjectResult
						.getAsJsonObject("best_location");
				JsonObject jsonObjectLocation = jElementLocation
						.getAsJsonObject();

				JsonElement jsonElementLatLong = jsonObjectLocation
						.getAsJsonObject("lat_long");
				JsonObject jsonObjectLatLong = jsonElementLatLong
						.getAsJsonObject();

				String latitude = jsonObjectLatLong.get("latitude").toString();
				String longitude = jsonObjectLatLong.get("longitude")
						.toString();

				// String city = jsonObjectLocation.get("address").toString();
				// String postalCode = jsonObjectLocation.get("postal_code")
				// .toString();

				logger.log(Level.INFO, "latitude: " + latitude);
				logger.log(Level.INFO, "longitude: " + longitude);

				// add to the datastore
				PhoneNumberLocation.createOrUpdatePhoneNumberLocation(
						phoneNumber, latitude, longitude);

			}

		} catch (Exception e) {
			e.printStackTrace();
		}

	}
}
