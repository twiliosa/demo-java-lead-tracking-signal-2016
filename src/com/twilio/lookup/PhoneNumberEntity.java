package com.twilio.lookup;

import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.common.collect.Iterators;
import com.twilio.streaming.TwilioLeadTrackingPullStats;
import com.twilio.util.Util;
import com.twilio.util.UtilHelper;

public class PhoneNumberEntity {

	private static final Logger logger = Logger
			.getLogger(PhoneNumberEntity.class.getCanonicalName());

	public static void createOrUpdatePhoneNumber(String phonenumber,
			String phonenumber_type, String phonenumber_carrier,
			String phonenumber_countrycode) {

		Entity phoneNumber = new Entity("phonenumber", phonenumber);
		phoneNumber.setProperty("phonenumber", phonenumber);
		phoneNumber.setProperty("phonenumber_type", phonenumber_type);
		phoneNumber.setProperty("phonenumber_carrier", phonenumber_carrier);
		phoneNumber.setProperty("phonenumber_countrycode",
				phonenumber_countrycode);
		Util.persistEntity(phoneNumber);

	}

	public static Iterable<Entity> getAllPhoneNumbers() {
		Iterable<Entity> entities = Util
				.listEntities("phonenumber", null, null);
		return entities;
	}

	public static Iterable<Entity> getPhoneNumber(String phoneNumber) {
		Iterable<Entity> entities = Util.listEntities("phonenumber",
				"phonenumber", phoneNumber);
		return entities;
	}

	public static Entity getSinglePhoneNumber(String phoneNumber) {
		Key key = KeyFactory.createKey("phonenumber", phoneNumber);
		return Util.findEntity(key);
	}

}
